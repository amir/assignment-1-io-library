%define SYSCALL_WRITE 1
%define STDOUT 1
%define SYSCALL_EXIT 60

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi], 0
    jz .end
    inc rax
    inc rdi
    jmp .loop
.end:
    ret
	
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdx, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    sub	rsp, 24
    mov	rax, rdi			; делимое
    mov	rcx, 23
    mov	rsi, 10				; делитель
    mov	byte [rsp+23], 0

.divide_loop:
    xor rdx, rdx            ; остаток от деления
    div rsi
    add	dl, '0'
    dec rcx
    mov [rsp+rcx], dl
    test rax, rax
    jnz .divide_loop

    lea rdi, [rsp+rcx]
    call print_string

    add rsp, 24
    ret
    
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi,rdi
    jns print_uint

    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    
    jmp print_uint



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.loop:
    mov al, [rdi]
    cmp al, [rsi]
    jne .not_equal
    inc rdi
    inc rsi
    test al, al
    jnz .loop
    mov rax, 1
    ret
.not_equal:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    sub rsp, 8
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    lea rsi, [rsp]
    syscall

    test rax, rax
    jz .eof
    mov al, [rsp]
	
.eof:
    add rsp, 8
    ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi        ; адрес начала буффера
    mov r13, rsi        ; размер буффера
    xor r14, r14        ; счётчик длины

.skip_spaces:
    call read_char
    cmp al, 0x20
    je .skip_spaces
    cmp al, 0x9
    je .skip_spaces
    cmp al, 0xA
    je .skip_spaces
    test al, al
    jz .eof

.prepare_buffer:
    test r13, r13
    jz .overflow
    dec r13			    ; выделение места для нуль-терминатора

.read_loop:
    mov [r12 + r14], al
    inc r14

    call read_char
    test al, al
    jz .end_word
    cmp al, 0x20
    je .end_word
    cmp al, 0x9
    je .end_word
    cmp al, 0xA
    je .end_word

    dec r13
    jz .overflow

    jmp .read_loop

.end_word:
    test r13, r13
    jz .overflow
    mov byte [r12 + r14], 0   ; завершение строки нулём
    mov rax, r12
    mov rdx, r14
    jmp .exit

.overflow:
    xor rax, rax
    jmp .exit

.eof:
    test r13, r13
    jz .overflow
    mov byte [r12 + r14], 0   ; завершение строки нулём
    xor rax, rax
    xor rdx, rdx

.exit:
    pop r14
    pop r13
    pop r12
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx

.parse_loop:
    movzx rcx, byte [rdi + rdx]
    test cl, cl
    jz .done

    sub cl, '0'
    cmp cl, 9
    ja .done

    imul rax, 10
    add rax, rcx

    inc rdx
    jmp .parse_loop

.done:
    ret

    
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    je .negative
    cmp byte [rdi], '+'
    je .positive
    jmp parse_uint

.positive:
    inc rdi
    jmp parse_uint

.negative:
    cmp byte [rdi + 1], 0
    je .error
    
    push rdi
    inc rdi
    call parse_uint
    pop rdi
    test rdx, rdx
    jz .error
    neg rax
    inc rdx
    ret

.error:
    xor rdx, rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push r12
    push r13
    sub rsp, 8
	
    mov r12, rdi        ; указатель на источник
    mov r13, rsi        ; указатель на буфер
    call string_length
    cmp rax, rdx
    jae .fail
	
    inc rax             ; + нуль-терминатор
    mov rdi, r13
    mov rsi, r12
    mov rcx, rax
    rep movsb           ; перемещение байта из [rsi] в [rdi] rcx раз
    mov byte [rdi], 0   ; нуль-терминатор
    add rsp, 8
    pop r13
    pop r12
    ret
.fail:
    xor rax, rax
    add rsp, 8
    pop r13
    pop r12
    ret